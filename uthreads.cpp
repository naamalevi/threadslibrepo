#include "uthreads.h"
#include "Thread.cpp"
#include <list>
#include <iostream>
#include <signal.h>
#include <sys/time.h>
#include <math.h>
#include <cstring>
#define MAX_USEC 1000000


std::list<Thread*> readyThreads;
Thread* allThreads[MAX_THREAD_NUM];
int totalQuantum;
int quantumTimeUsec;
int quantumTimeSec;
int numOfLiveThreads;
Thread* runningThread;
struct sigaction sa;
struct itimerval timer;
sigset_t set;



// function declarations:
/** Returns the minimal ID that is not taken by another existiong thread.
 * Return value: an int representing the minimal ID
 */
int getFreeID();

/* Releases all the memory that was allocated by the program. */
void releaseAllMemo();

/**
 * Releases all the threads that called sync on the thread pointed by pThread,
 * the threads will not be in SYNC state any more, and if they are not blocked
 * they will be added to the ready threads list.
 * @param pThread a pointer to a the thread that the threads called sync on.
 */
void releaseDependentThreads(Thread *pThread);

/**
 * A scheduling function, decided which thread will run next according to the required
 * algorithm, and performs the switching.
 */
void scheduler();

/**
 * SIGVTALRM handler function. This function will be called whenever the timer expires.
 * @param sig an int representing the signal.
 */
void quantumExpired(int sig);

/**
 * checks if an alloc action succeeded.
 * @param addr an address that was returned by an alloc action.
 */
void checkAllocSuccess(void* addr);


#ifdef __x86_64__
/* code for 64 bit Intel arch */

typedef unsigned long address_t;
#define JB_SP 6
#define JB_PC 7

/* A translation is required when using an address of a variable.
   Use this as a black box in your code. */
address_t translate_address(address_t addr)
{
    address_t ret;
    asm volatile("xor    %%fs:0x30,%0\n"
            "rol    $0x11,%0\n"
    : "=g" (ret)
    : "0" (addr));
    return ret;
}

#else
/* code for 32 bit Intel arch */

typedef unsigned int address_t;
#define JB_SP 4
#define JB_PC 5

/* A translation is required when using an address of a variable.
   Use this as a black box in your code. */
address_t translate_address(address_t addr)
{
    address_t ret;
    asm volatile("xor    %%gs:0x18,%0\n"
		"rol    $0x9,%0\n"
                 : "=g" (ret)
                 : "0" (addr));
    return ret;
}

#endif

/*
 * Description: This function initializes the thread library.
 * You may assume that this function is called before any other thread library
 * function, and that it is called exactly once. The input to the function is
 * the length of a quantum in micro-seconds. It is an error to call this
 * function with non-positive quantum_usecs.
 * Return value: On success, return 0. On failure, return -1.
*/
int uthread_init(int quantum_usecs)
{
    if (sigemptyset(&set))
    {
        std::cerr << "system error: sigemptyset error." << std::endl;
        // no memory should be freed.
        exit(1);
    }
    if (sigaddset(&set, SIGVTALRM))
    {
        std::cerr << "system error: sigaddset error." << std::endl;
        // no memory should be freed.
        exit(1);
    }
    if (sigprocmask(SIG_BLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        // no memory should be freed.
        exit(1);
    }
    if (quantum_usecs <= 0)
    {
        std::cerr << "thread library error: quantum_usecs should be positive" << std::endl;
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            // no memory should be freed.
            exit(1);
        }
        return -1;
    }
    quantumTimeSec = 0;
    quantumTimeUsec = quantum_usecs;
    if (quantum_usecs >= MAX_USEC)
    {
        quantumTimeSec = (int)floor(quantum_usecs/MAX_USEC);
        quantumTimeUsec = quantum_usecs % MAX_USEC;
    }

    // Install timer_handler as the signal handler for SIGVTALRM.
    sa.sa_handler = &quantumExpired;
    if (sigaction(SIGVTALRM, &sa, NULL))
    {
        std::cerr << "system error: sigaction error." << std::endl;
        // no memory should be freed.
        exit(1);
    }

    // Configure the timer to expire after quantum_usecs sec... */
    timer.it_value.tv_sec = quantumTimeSec;		// first time interval, seconds part
    timer.it_value.tv_usec = quantumTimeUsec;		// first time interval, microseconds part

    // configure the timer to expire every quantum_usecs sec after that.
    timer.it_interval.tv_sec = quantumTimeSec;	// following time intervals, seconds part
    timer.it_interval.tv_usec = quantumTimeUsec;	// following time intervals, microseconds part

    // Start a virtual timer. It counts down whenever this process is executing.
    if (setitimer(ITIMER_VIRTUAL, &timer, NULL))
    {
        std::cerr << "system error: setitimer error." << std::endl;
        // no memory should be freed.
        exit(1);
    }
    Thread* newThread = new (std::nothrow) Thread(0, nullptr);
    checkAllocSuccess(newThread);
    newThread->setState(RUNNING);
    newThread->setNumOfQuantum(1);
    allThreads[0] = newThread;
    runningThread = newThread;
    numOfLiveThreads = 1;
    totalQuantum = 1;
    if (sigprocmask(SIG_UNBLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    return 0;
}

/*
 * Description: This function creates a new thread, whose entry point is the
 * function f with the signature void f(void). The thread is added to the end
 * of the READY threads list. The uthread_spawn function should fail if it
 * would cause the number of concurrent threads to exceed the limit
 * (MAX_THREAD_NUM). Each thread should be allocated with a stack of size
 * STACK_SIZE bytes.
 * Return value: On success, return the ID of the created thread.
 * On failure, return -1.
*/
int uthread_spawn(void (*f)(void))
{
    sigprocmask(SIG_BLOCK, &set, NULL);
    if (sigprocmask(SIG_BLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo(); // todo: block sigalarm inside release ?
        exit(1);
    }
    if (numOfLiveThreads >= MAX_THREAD_NUM)
    {
        std::cerr << "thread library error: too many threads" << std::endl;
        sigprocmask(SIG_UNBLOCK, &set, NULL);
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            releaseAllMemo();
            exit(1);
        }
        return -1;
    }
    address_t sp, pc;
    int id = getFreeID();
    char* threadStack = new (std::nothrow) char[STACK_SIZE];
    checkAllocSuccess(threadStack);
    Thread* newThread = new (std::nothrow) Thread(id, threadStack);
    checkAllocSuccess(newThread);
    sp = (address_t)threadStack + STACK_SIZE - sizeof(address_t);
    pc = (address_t)f;
    sigsetjmp(newThread->_env, 1);
    (newThread->_env->__jmpbuf)[JB_SP] = translate_address(sp);
    (newThread->_env->__jmpbuf)[JB_PC] = translate_address(pc);
    if (sigemptyset(&newThread->_env->__saved_mask))
    {
        std::cerr << "system error: sigemptyset error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    allThreads[id] = newThread;
    numOfLiveThreads++;
    try
    {
        readyThreads.push_back(newThread);
    }
    catch (const std::bad_alloc &)
    {
        checkAllocSuccess(nullptr);
    }
    if (sigprocmask(SIG_UNBLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    return id;
}

/*
 * Description: This function terminates the thread with ID tid and deletes
 * it from all relevant control structures. All the resources allocated by
 * the library for this thread should be released. If no thread with ID tid
 * exists it is considered an error. Terminating the main thread
 * (tid == 0) will result in the termination of the entire process using
 * exit(0) [after releasing the assigned library memory].
 * Return value: The function returns 0 if the thread was successfully
 * terminated and -1 otherwise. If a thread terminates itself or the main
 * thread is terminated, the function does not return.
*/
int uthread_terminate(int tid)
{
    if (sigprocmask(SIG_BLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    if (allThreads[tid] == nullptr)
    {
        std::cerr << "thread library error: no such thread" << std::endl;
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            releaseAllMemo();
            exit(1);
        }
        return -1;
    }
    if (tid == 0)
    {
        // main is terminated
        releaseAllMemo();
        exit(0);
    }
    Thread* terminatedThread = allThreads[tid];
    releaseDependentThreads(terminatedThread);
    for ( int id = 0; id < MAX_THREAD_NUM; id++)
    {
        if (allThreads[id] != nullptr)
        {
            allThreads[id]->_syncDependents.remove(terminatedThread);
        }
    }
    int state = terminatedThread->getState();
    if (state == READY)
    {
        readyThreads.remove(terminatedThread);
    }
    allThreads[tid] = nullptr;
    numOfLiveThreads--;
    delete terminatedThread->getStackBase();
    delete terminatedThread;
    if (state == RUNNING)
    {
        runningThread = nullptr;
        scheduler();
    }
    if (sigprocmask(SIG_UNBLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    return 0;
}

/*
 * Description: This function blocks the thread with ID tid. The thread may
 * be resumed later using uthread_resume. If no thread with ID tid exists it
 * is considered as an error. In addition, it is an error to try blocking the
 * main thread (tid == 0). If a thread blocks itself, a scheduling decision
 * should be made. Blocking a thread in BLOCKED state has no
 * effect and is not considered an error.
 * Return value: On success, return 0. On failure, return -1.
*/
int uthread_block(int tid)
{
    if (sigprocmask(SIG_BLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    if (allThreads[tid] == nullptr)
    {
        std::cerr << "thread library error: no such thread" << std::endl;
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            releaseAllMemo();
            exit(1);
        }
        return -1;
    }
    if (tid == 0)
    {
        // main is blocked
        releaseAllMemo();
        exit(0);
    }
    Thread* blockedThread = allThreads[tid];
    int oldState = blockedThread->getState();
    int newState = (oldState == SYNC) ? SYNC_AND_BLOCKED : BLOCKED;
    blockedThread->setState(newState);
    if (oldState == RUNNING)
    {
        scheduler();
    }
    if (sigprocmask(SIG_UNBLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    return 0;
}

/*
 * Description: This function resumes a blocked thread with ID tid and moves
 * it to the READY state if it's not synced. Resuming a thread in a RUNNING or READY state
 * has no effect and is not considered as an error. If no thread with
 * ID tid exists it is considered an error.
 * Return value: On success, return 0. On failure, return -1.
*/
int uthread_resume(int tid)
{
    if (sigprocmask(SIG_BLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    if (allThreads[tid] == nullptr)
    {
        std::cerr << "thread library error: no such thread" << std::endl;
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            releaseAllMemo();
            exit(1);
        }
        return -1;
    }
    if (allThreads[tid]->getState() == BLOCKED)
    {
        allThreads[tid]->setState(READY);
        try
        {
            readyThreads.push_back(allThreads[tid]);
        }
        catch (const std::bad_alloc &)
        {
            checkAllocSuccess(nullptr);
        }
    }
    if (allThreads[tid]->getState() == SYNC_AND_BLOCKED)
    {
        allThreads[tid]->setState(SYNC);
    }
    if (sigprocmask(SIG_UNBLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    return 0;
}

/*
 * Description: This function blocks the RUNNING thread until thread with
 * ID tid will terminate. It is considered an error if no thread with ID tid
 * exists, if thread tid calls this function or if the main thread (tid==0) calls this function.
 * Immediately after the RUNNING thread transitions to the BLOCKED state a scheduling decision
 * should be made.
 * Return value: On success, return 0. On failure, return -1.
*/
int uthread_sync(int tid)
{
    if (sigprocmask(SIG_BLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    if (allThreads[tid] == nullptr)
    {
        std::cerr << "thread library error: no such thread" << std::endl;
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            releaseAllMemo();
            exit(1);
        }
        return -1;
    }
    if (uthread_get_tid() == 0)
    {
        std::cerr << "thread library error: main can't sync" << std::endl;
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            releaseAllMemo();
            exit(1);
        }
        return -1;
    }
    if (uthread_get_tid() == tid)
    {
        std::cerr << "thread library error: can't use sync on the running thread" << std::endl;
        if (sigprocmask(SIG_UNBLOCK, &set, NULL))
        {
            std::cerr << "system error: sigprocmask error." << std::endl;
            releaseAllMemo();
            exit(1);
        }
        return -1;
    }
    try
    {
        allThreads[tid]->_syncDependents.push_back(runningThread);
    }
    catch (const std::bad_alloc &)
    {
        checkAllocSuccess(nullptr);
    }
    runningThread->setState(SYNC); // no sync after blocked, sync only occurs in running state
    scheduler();
    if (sigprocmask(SIG_UNBLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    return 0;
}

/*
 * Description: This function returns the thread ID of the calling thread.
 * Return value: The ID of the calling thread.
*/
int uthread_get_tid()
{
    return runningThread->getThreadID();
}

/*
 * Description: This function returns the total number of quantums since
 * the library was initialized, including the current quantum.
 * Right after the call to uthread_init, the value should be 1.
 * Each time a new quantum starts, regardless of the reason, this number
 * should be increased by 1.
 * Return value: The total number of quantums.
*/
int uthread_get_total_quantums()
{
    return totalQuantum;
}

/*
 * Description: This function returns the number of quantums the thread with
 * ID tid was in RUNNING state. On the first time a thread runs, the function
 * should return 1. Every additional quantum that the thread starts should
 * increase this value by 1 (so if the thread with ID tid is in RUNNING state
 * when this function is called, include also the current quantum). If no
 * thread with ID tid exists it is considered an error.
 * Return value: On success, return the number of quantums of the thread with ID tid.
 * 			     On failure, return -1.
*/
int uthread_get_quantums(int tid)
{
    return allThreads[tid]->getNumOfQuantum();
}




// sigalarm handler
void quantumExpired(int sig)
{
    if (sigprocmask(SIG_BLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
    runningThread->setState(READY);
    try
    {
        readyThreads.push_back(runningThread);
    }
    catch (const std::bad_alloc &)
    {
        checkAllocSuccess(nullptr);
    }
    scheduler();
    if (sigprocmask(SIG_UNBLOCK, &set, NULL))
    {
        std::cerr << "system error: sigprocmask error." << std::endl;
        releaseAllMemo();
        exit(1);
    }
}

void scheduler()
{
    if (runningThread != nullptr) // if runningThread == nullptr then the running thread has terminated
    {
        int ret_val = sigsetjmp(runningThread->_env,1);
        if (ret_val == 1)
        {
            return;
        }
    }
    runningThread = readyThreads.front();
    readyThreads.pop_front();
    runningThread->setState(RUNNING);
    runningThread->incNumOfQuantum();
    totalQuantum++;

    // Start a virtual timer again. It counts down whenever this process is executing.
    timer.it_value.tv_sec = quantumTimeSec;
    timer.it_value.tv_usec = quantumTimeUsec;
    if (setitimer (ITIMER_VIRTUAL, &timer, NULL))
    {
        std::cerr << "system error: setitimer error." << std::endl;
        exit(1);
    }
    siglongjmp(runningThread->_env,1);
}

int getFreeID()
{
    for (int i = 0; i < MAX_THREAD_NUM; i++)
    {
        if (allThreads[i] == nullptr)
        {
            return i;
        }
    }
    return -1;
}

void releaseDependentThreads(Thread *pThread)
{
    while (!(pThread->_syncDependents.empty()))
    {
        Thread* toPush = pThread->_syncDependents.front();
        if (toPush->getState() == SYNC_AND_BLOCKED)
        {
            toPush->setState(BLOCKED);
        } else
        {
            toPush->setState(READY);
            try
            {
                readyThreads.push_back(toPush);
            }
            catch (const std::bad_alloc &)
            {
                checkAllocSuccess(nullptr);
            }
        }
        pThread->_syncDependents.pop_front();
    }
}

void releaseAllMemo()
{
    for (int i = 0; i< MAX_THREAD_NUM; i++)
    {
        if (allThreads[i] != nullptr)
        {
            if (i != 0) // main thread's stack should not be deleted - because it was not dynamically allocated
            {
                delete allThreads[i]->getStackBase();
            }
            delete allThreads[i];
            allThreads[i] = nullptr;
        }
    }
    readyThreads.clear();
}

void checkAllocSuccess(void* addr)
{
    if (addr == nullptr)
    {
        std::cerr << "system error: out of space" << std::endl;
        releaseAllMemo();
        exit(1);

    }
}
