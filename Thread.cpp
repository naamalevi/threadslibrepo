#include <list>
#include <setjmp.h>

typedef unsigned long address_t;

#define RUNNING 1
#define READY 2
#define BLOCKED 3
#define SYNC 4
#define SYNC_AND_BLOCKED 5

/**
 * A Thread class, represents a user level thread in the uthread library.
 */
class Thread
{
public:
    sigjmp_buf _env;
    std::list<Thread*> _syncDependents; // all threads that are waiting until this thread is done.

    /**
     * class constructor
     * @param id the id of the new created thread.
     * @param stackBase a pointer to a place in the memory that will be the new thread's stack base.
     */
    Thread(int id, char* stackBase)
    {
        _id = id;
        _stackBase = stackBase;
        _state = READY;
        _numOfQuantum = 0;
    }

    /**
     * @return an int representing this thread's ID
     */
    int getThreadID()
    {
        return _id;
    }

    /**
     * @return a pointer to a place in the memory representing this thread's stack base.
     */
    char* getStackBase()
    {
        return _stackBase;
    }

    /**
     * @return an int representing this thread's State
     */
    int getState()
    {
        return _state;
    }

    /**
     * Sets this thread's state to the given state.
     * @param newState an int representing this thread's new state.
     */
    void setState(int newState)
    {
        _state = newState;
    }

    /**
     * @return an int representing this thread's numOfQuantums.
     */
    int getNumOfQuantum()
    {
        return _numOfQuantum;
    }

    /**
     * Increases this thread's numOfQuantums by one.
     */
    void incNumOfQuantum()
    {
        _numOfQuantum++;
    }

    /**
     * Sets this thread's NumOfQuantums to the given number.
     * @param newNum an int representing this thread's new numOfQuantums.
     */
    void setNumOfQuantum(int newNum)
    {
        _numOfQuantum = newNum;
    }


private:
    int _id;
    char* _stackBase;
    int _state;
    int _numOfQuantum;

};